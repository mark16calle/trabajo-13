from Pila import  Pila


def calculadora_polaca(elementos):
    """ Dada una lista de elementos que representan las componentes de
    una expresión en notacion polaca inversa, evalúa dicha expresión.Si la expresion está mal formada, levanta ValueError. """

    p = Pila()
    c=0
    for elemento in elementos:
        #print ("DEBUG: "+ elemento)
        # Intenta convertirlo a número
        try:
            numero = float(elemento)
            p.apilar(numero)
            c=c+1
            print ("DEBUG: apila la   "+ str(c) + "  posicion y  valor es:  "+   str(numero))
        # Si no se puede convertir a número, debería ser un operando
        except ValueError:
            # Si no es un operando válido, levanta ValueError
            if elemento not in "+-*/ %" or len(elemento) != 1:
                raise ValueError("Operando inválido")
            # Si es un operando válido, intenta desapilar y operar
            try:
                a1 = p.desapilar()
                print ("DEBUG: desapila  "+ str(a1))
                a2 = p.desapilar()
                print ("DEBUG: desapila  "+ str(a2))
                # Si hubo problemas al desapilar
            except ValueError:
                print ("DEBUG: error pila faltan operandos")
                raise ValueError("Faltan operandos")

            if elemento == "+":
                resultado = a2 + a1
            elif elemento == "-":
                resultado = a2 - a1
            elif elemento == "*":
                resultado = a2 * a1
            elif elemento == "/":
                resultado = a2 / a1
            elif elemento == " %":
                resultado = a2 % a1
            print ("DEBUG: apila "+ str(resultado))
            p.apilar(resultado)
    # Al final, el resultado debe ser lo único en la Pila
    res = p.desapilar()
    if p.es_vacia():
        print ("DEBUG: pila vacia")
        return res
    else:
        print ("DEBUG: error pila sobran operandos")
        raise ValueError("Sobran operandos")
    
op=""
    
while op!="s" and op!="S":
    print("------------------------------------------")
    print("\t\tMENU\t\t")
    print("(C) CONVERTIR EXPRESION SUFIJA A INFIJA")
    print("(P) PROCESAROPERACION POLACA")
    print("(S)SALIR")
    print("NOTA: para multiplicar matrices primero debe cargar la matriz")
    op= input("Ingrese una opcion v?lida: ")

    if op=="c" or op=="C":
        stack = []

#expresion = "AB+CD+*"
        expresion = input("digita la expresion polaca sin espacios :  ")

        for carac in expresion:
            print (carac)
            if carac == "+" or carac == "*" or carac == "-" or carac == "/" or carac == "%":
                a = stack.pop()
                b = stack.pop()
                stack.append("(" + b + carac + a + ")")
            else: # Apilo caracteres
                stack.append(carac)
            
        print (stack.pop())
                
                

    elif op=="p" or op=="P":

        expresion = input("digita la expresion polaca con espacios :  ")
        elementos = expresion.split()
        resultado=calculadora_polaca(elementos)
        print ("el resultado es : "+ str(resultado))


    else:
        print("Digite una opcion valida \n")


exit(0)

